# Microservice Assessment for Full Stack positions

Imagine that your requirement is to create a ZIP file which contains the following structure

```
Project-Name
    Category-01
        Enumeration-01
            File-name-01-01-001.jpg
            File-name-01-01-002.jpg
            File-name-01-01-003.jpg
        Enumeration-02
            File-name-01-02-001.jpg
            File-name-01-02-002.jpg
            File-name-01-02-003.jpg
            File-name-01-02-005.jpg
            File-name-01-02-006.jpg
    Category-02
        Enumeration-01
            File-name-02-01-001.jpg
        Enumeration-02
            File-name-02-02-001.jpg
            File-name-02-02-002.jpg
        Enumeration-03
            File-name-02-03-001.jpg
            File-name-02-03-002.jpg
            File-name-02-03-003.jpg
        Enumeration-04
            File-name-02-04-001.jpg
            File-name-02-04-002.jpg
            File-name-02-04-003.jpg
            File-name-02-04-004.jpg
```

**Where:**

 - `Project-Name` is a non-whitespaced project name, case-insensitive
 - `Category-{NUMBER}` - where `{NUMBER}` is a category number or ID
 - `Enumeration-{NUMBER}` - where `{NUMBER}` is an enumeration number or ID
 - `File-name-{CATEGORY}-{ENUM}-{INDEX}` - where `{CATEGORY}` is a category number or ID, `{ENUM}` is an enumeration number or ID, `{INDEX}` is the non-zero index value for a specific item or a file.

To have the structure fromn above in the resulting ZIP file is the primary and sole goal of this task.

Categories and enumerations are not available per-project, but rather are created once and available for all existing and future projects.

**Achieve the following:**

 - Allow creation of categories
 - Allow creation of enumerations
 - Allow creation of projects
 - Allow uploading files which belong to specific projects
 - Do not modify file contents in any way imaginable (only create copies from the originals, if need be)
 - Store as much as meta information you can about a specific upload (original filename, file size, mime type, original constraints, etc.)
 - Create an on-demand endpoint which will create a new ZIP file ZIP containing all the files in the structure from above, and send it as a download
 - Create the most simple Web interface in any frontend technology you like (it can be non-MVVM/vDOM)
 - Create an upload form in which you can specify a new project name or chose an existing project, after that users must select a category and an enumeration where to uplaod new files
 - Allow files to be deleted and/or moved from an enumeration to another enumeration, and even a project

**Consider the following:**

 - Uploaded files might be larger than 10 MB, each - think about timeouts
 - Your hard-drive/space resources are limited
 - Uploaded files are not compressed or modified in any way AND they should and cannot be modified/compressed
 - You must create a seamless experience for most of the time
 - Consider best way to create ZIP files and make download process seamless
 - At one point you will run out of storage - how to cope with this?
 - After each new upload, deletion or file move to another enumeration, you must re-order file names in the current folder

**Delivery options:**
 
 - Deliver solution as a Git repository: GitHub, GitLab, etc.

**Bonuses:**

 - Dockerization
 - Create a staging environment for testing and preview
 - API documentation for specific endpoints
 - Create 5 projects, 5 categories and 5 enumerations, and 1000 dummy files (can be duplicates) with file size larger than 5 MB and lower than 10 MB, and spread them randomly across those projects, categories and enumerations.

**Important note:**

- Each project can have multiple categories, and those categories can have multiple enumerations, and those enumerations can have many files.
